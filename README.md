# Implementierung von Informationssystemen

## Development

### Tools to install before you start working

- [.editorconfig](https://editorconfig.org/#download): please install the plugin to make sure you use the same code style
- [pre-commit](https://pre-commit.com/#install): please install pre-commit to make sure you use the same code style before pushing to Git. After installing please run `pre-commit install`. You can test the installation with `pre-commit run -a`.
- Docker
- Docker-Compose
- Bash
- Make (or run the raw commands inside a bash)

### Start applications/containers

Startup:

```shell
  docker-compose up
```

Alternatively you can run the following command:

```shell
  make up
```

### Bootstrapping Camunda

With the following script, all users, groups, authorizations gets created:

```shell
  ./scripts/run.sh
```

Alternatively you can run the following command:

```shell
  make init
```

### Deployment

With the following command, the application gets deployed:

```shell
  ./scripts/clean.sh
  ./scripts/deploy.sh
```

Alternatively you can run the following command:

```shell
  make deploy
```

### Run Camunda and WildFly in Docker (with docker-compose)

`docker-compose up -d` and open your browser with url: http://localhost:8080/camunda-welcome/index.html.
For WildFly open `http://localhost:9990/console/index.html`.

With this, a user (admin/admin) for WildFly gets created.
Users for camunda have to be created manually.
There would be an option to automate this via Camunda's rest api.

All data under path `/camunda` gets stored in a docker volume.

## Troubleshooting

- Arch Linux fix wrong `java -version`: https://wiki.archlinux.org/title/java#Switching_between_JVM

