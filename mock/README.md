# Mock api

This mock got created for simulating an API to "publish" a new Stelle(nauschreibung) for a company.
This was implemented by a simple rest endpoint written in Golang because of simplicity.

# Disclaimer

Don't judge for the quality of the code and the used language/frameworks of this mock!

# Author

seg0979-Sebastian Gaiser
