package main

import (
  "fmt"
  "github.com/gin-gonic/gin"
  "github.com/google/uuid"
  "time"
)

type Store struct {
  db map[string]string
}

func NewStore() *Store {
  store := &Store{db: map[string]string{}}
  return store
}
func (k Store) Get(key string) string {
  return k.db[key]
}
func (k Store) Put(key string, value string) {
  k.db[key] = value
}

func (k Store) Delete(key string) {
  delete(k.db, key)
}

type Stelle struct {
  Name string `json:"name" binding:"required"`
}

type Id struct {
  WebsiteUuid string `json:"websiteuuid" binding:"required"`
}

func main() {
  store := NewStore()
  fmt.Println("Hello World from Mock Server")
  r := gin.Default()

  r.POST("/", func(c *gin.Context) {
    c.JSON(200, gin.H{
      "message": "success",
    })
  })

  r.POST("/stelle", func(c *gin.Context) {
    stelle := &Stelle{}
    err := c.BindJSON(stelle)
    if err != nil {
      c.JSON(400, gin.H{"error": err.Error()})
      return
    }
    time.Sleep(time.Second * 2)
    websiteUuid := uuid.New().String()
    store.Put(websiteUuid, stelle.Name)
    fmt.Println("Name: " + stelle.Name + " with UUID: " + websiteUuid)
    c.JSON(200, gin.H{
      "stelle": stelle.Name,
      "uuid":   websiteUuid,
    })
  })

  r.POST("/stelle/close", func(c *gin.Context) {
    websiteUuid := &Id{}
    err := c.BindJSON(websiteUuid)
    if err != nil {
      c.JSON(400, gin.H{"error": err.Error()})
      return
    }
    fmt.Println("Try to close with UUID: " + websiteUuid.WebsiteUuid)
    store.Delete(websiteUuid.WebsiteUuid)
    c.JSON(200, gin.H{
      "stelle": "",
      "uuid":   websiteUuid.WebsiteUuid,
    })
  })
  err := r.Run()
  if err != nil {
    return
  }
}
