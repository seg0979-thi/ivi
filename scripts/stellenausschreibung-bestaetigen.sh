#!/bin/bash

# Created by seg0979; Sebastian Gaiser
# Get task id
tid=$(curl -s -H "Content-Type: application/json" -X POST -d '{"processDefinitionNameLike": "Ausschreibung der Stelle"}' -u demo:demo "http://localhost:8080/engine-rest/history/process-instance" | jq -r '.[].id')

if [ -z "$tid" ]; then
  echo "No tasks found"
  exit 1
else
  echo "Task found with id $tid"
  curl -s -H "Content-Type: application/json" -X POST -d '{"messageName": "Bestaetigung", "processInstanceId": "'"${tid}"'", "processVariables" : { "stellenausschreibungwebsiteid" : { "value" : "dies ist eine id", "type" : "String" }}}' -u demo:demo "http://localhost:8080/engine-rest/message"
fi
