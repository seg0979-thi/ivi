#!/bin/bash

# Created by seg0979; Sebastian Gaiser
# Get deployment list
list=$(curl -s -H "Content-Type: application/json" -X GET -u demo:demo http://localhost:8080/engine-rest/deployment | jq -r '.[].id')

# Clean deployments
if [ -z "$list" ]
then
  echo "No deployments found"
else
  echo "List contains:"
  echo "$list"
  echo ""

  IFS='
  ' # split on newline only
  set -o noglob
  for id in $list
  do
    echo 'Undeploying' "${id}"
    curl -s -H "Content-Type: application/json" -X DELETE -u demo:demo "http://localhost:8080/engine-rest/deployment/${id}?cascade=true&skipCustomListeners=true&skipIoMappings=true"
  done
fi
