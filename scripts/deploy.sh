#!/bin/bash

# Created by seg0979; Sebastian Gaiser
# Deploys the application to WildFly server
echo "Try to deploy application..."
mvn clean install wildfly:deploy
echo "Deployed"
