#!/bin/bash

# Für den Prozess notwendige Daten übernehmen
echo "Gebe Prozessinstanz-Id ein:"
read -r processInstanceId

#Message 
curl -H "Content-Type: application/json" -X POST -d '
{
    "messageName": "Arbeitsvertrag",
    "processInstanceId" : "'"${processInstanceId}"'",
    "resultEnabled" : true
}' "http://localhost:8080/engine-rest/message"

# read -p -r "Enter zum Beenden"
# exit 0
