#!/bin/bash

# Created by seg0979; Sebastian Gaiser
# Telemetry
curl -H "Content-Type: application/json" -X POST -d '{"enableTelemetry":false}' -u demo:demo http://localhost:8080/engine-rest/telemetry/configuration

# Groups
curl -H "Content-Type: application/json" -X POST -d '{"id":"normal", "name":"Normal User", "type":"WORKFLOW"}' -u demo:demo http://localhost:8080/engine-rest/group/create
curl -H "Content-Type: application/json" -X POST -d '{"id":"fuehrungskraft", "name":"Fuehrungskraft", "type":"WORKFLOW"}' -u demo:demo http://localhost:8080/engine-rest/group/create
curl -H "Content-Type: application/json" -X POST -d '{"id":"teamleiter", "name":"Teamleiter", "type":"WORKFLOW"}' -u demo:demo http://localhost:8080/engine-rest/group/create
curl -H "Content-Type: application/json" -X POST -d '{"id":"recruiter", "name":"Recruiter", "type":"WORKFLOW"}' -u demo:demo http://localhost:8080/engine-rest/group/create

# Application authorization
curl -H "Content-Type: application/json" -X POST -d '{"type" : 1, "permissions": ["ACCESS"], "userId": null, "groupId": "normal", "resourceType": 0, "resourceId": "tasklist"}' -u demo:demo http://localhost:8080/engine-rest/authorization/create
curl -H "Content-Type: application/json" -X POST -d '{"type" : 1, "permissions": ["ACCESS"], "userId": null, "groupId": "fuehrungskraft", "resourceType": 0, "resourceId": "tasklist"}' -u demo:demo http://localhost:8080/engine-rest/authorization/create
curl -H "Content-Type: application/json" -X POST -d '{"type" : 1, "permissions": ["ACCESS"], "userId": null, "groupId": "teamleiter", "resourceType": 0, "resourceId": "tasklist"}' -u demo:demo http://localhost:8080/engine-rest/authorization/create
curl -H "Content-Type: application/json" -X POST -d '{"type" : 1, "permissions": ["ACCESS"], "userId": null, "groupId": "recruiter", "resourceType": 0, "resourceId": "tasklist"}' -u demo:demo http://localhost:8080/engine-rest/authorization/create

# Users
curl -H "Content-Type: application/json" -X POST -d '{"profile": {"id": "fozzie", "firstName":"Fozzie", "lastName":"Bear", "email":"fozzie@example.com"}, "credentials": {"password":"fozzie"} }' -u demo:demo http://localhost:8080/engine-rest/user/create
curl -H "Content-Type: application/json" -X POST -d '{"profile": {"id": "kermit", "firstName":"Kermit", "lastName":"The Frog", "email":"kermit@example.com"}, "credentials": {"password":"kermit"} }' -u demo:demo http://localhost:8080/engine-rest/user/create
curl -H "Content-Type: application/json" -X POST -d '{"profile": {"id": "fridolin", "firstName":"Fridolin", "lastName":"Keller", "email":"fridolin@example.com"}, "credentials": {"password":"fridolin"} }' -u demo:demo http://localhost:8080/engine-rest/user/create
curl -H "Content-Type: application/json" -X POST -d '{"profile": {"id": "torben", "firstName":"Torben", "lastName":"Ludwig", "email":"torben@example.com"}, "credentials": {"password":"torben"} }' -u demo:demo http://localhost:8080/engine-rest/user/create
curl -H "Content-Type: application/json" -X POST -d '{"profile": {"id": "rudi", "firstName":"Rudi", "lastName":"Ratlos", "email":"rudi@example.com"}, "credentials": {"password":"rudi"} }' -u demo:demo http://localhost:8080/engine-rest/user/create

# User to Groups
curl -H "Content-Type: application/json" -X PUT -u demo:demo http://localhost:8080/engine-rest/group/normal/members/fozzie
curl -H "Content-Type: application/json" -X PUT -u demo:demo http://localhost:8080/engine-rest/group/camunda-admin/members/kermit
curl -H "Content-Type: application/json" -X PUT -u demo:demo http://localhost:8080/engine-rest/group/fuehrungskraft/members/fridolin
curl -H "Content-Type: application/json" -X PUT -u demo:demo http://localhost:8080/engine-rest/group/teamleiter/members/torben
curl -H "Content-Type: application/json" -X PUT -u demo:demo http://localhost:8080/engine-rest/group/recruiter/members/rudi
