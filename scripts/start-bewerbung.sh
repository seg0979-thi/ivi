#!/bin/bash

# Für den Prozess notwendige Daten übernehmen
echo "Gebe Anschreiben-Text ein:"
read -r anschreiben
#echo "Ihre Eingabe zum Anschreiben: ${anschreiben}"

echo "Gebe Lebenslauf-Text ein:"
read -r lebenslauf
#echo "Ihre Eingabe zum Lebenslauf: ${lebenslauf}"

echo "Gebe Referenzen-Text ein:"
read -r referenzen
#echo "Ihre Eingabe zu den Referenzen: ${referenzen}"

echo "Gebe StellenausschreibungId ein:"
read -r stellenausschreibungId

#Den Prozess mit den übernommenen Daten starten
curl -H "Content-Type: application/json" -X POST -d '
{
    "messageName": "Bewerbung",
    "businessKey" : "sid-C54C55CC-39FE-4FE1-AC21-88BE6D9804FA",
    "processVariables" : {
        "anschreiben" : {"value" : "'"${anschreiben}"'", "type": "String", "valueInfo" : { "transient" : false }},
        "lebenslauf" : {"value" : "'"${lebenslauf}"'", "type": "String", "valueInfo" : { "transient" : false }},
        "referenzen" : {"value" : "'"${referenzen}"'", "type": "String", "valueInfo" : { "transient" : false }},
        "stellenausschreibungId" : {"value" : "'"${stellenausschreibungId}"'", "type": "String", "valueInfo" : { "transient" : false }}
    },
    "resultEnabled" : true
}' "http://localhost:8080/engine-rest/message"

# read -p -r "Enter zum Beenden"
# exit 0
