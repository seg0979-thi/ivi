ARG VERSION=7.16.0
FROM camunda/camunda-bpm-platform:wildfly-$VERSION
LABEL maintainer="seg0979 Sebastian Gaiser"

ARG WILDFLY_USERNAME=admin
ARG WILDFLY_PASSWORD=admin
RUN /camunda/bin/add-user.sh ${WILDFLY_USERNAME} ${WILDFLY_PASSWORD}
