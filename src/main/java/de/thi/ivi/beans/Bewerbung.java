package de.thi.ivi.beans;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/** @author seg0979 Sebastian Gaiser */
/** @author tog8596 Tobias Gaul */
@Entity
@NamedQuery(
    name = Bewerbung.searchBewerbungById,
    query = "SELECT b FROM Bewerbung b WHERE b.bewerbungsid =:" + " bewerbungsid")
@Table(name = "bewerbung")
public class Bewerbung implements Serializable {
  private static final long serialVersionUID = 1L;

  public static final String searchBewerbungById = "Bewerbung.searchBewerbungById";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long bewerbungsid;

  private String anschreiben;
  private String lebenslauf;
  private String referenzen;
  private Long stellenausschreibungid;

  public Bewerbung(
      String anschreiben, String lebenslauf, String referenzen, Long stellenausschreibungid) {
    this.anschreiben = anschreiben;
    this.lebenslauf = lebenslauf;
    this.referenzen = referenzen;
    this.stellenausschreibungid = stellenausschreibungid;
  }

  public Bewerbung() {
    this.anschreiben = null;
    this.lebenslauf = null;
    this.referenzen = null;
    this.stellenausschreibungid = null;
  }

  public Long getBewerbungsid() {
    return bewerbungsid;
  }

  public void setBewerbungsid(Long bewerbungsid) {
    this.bewerbungsid = bewerbungsid;
  }

  public String getAnschreiben() {
    return anschreiben;
  }

  public void setAnschreiben(String anschreiben) {
    this.anschreiben = anschreiben;
  }

  public String getLebenslauf() {
    return lebenslauf;
  }

  public void setLebenslauf(String lebenslauf) {
    this.lebenslauf = lebenslauf;
  }

  public String getReferenzen() {
    return referenzen;
  }

  public void setReferenzen(String referenzen) {
    this.referenzen = referenzen;
  }

  public Long getStellenausschreibungid() {
    return stellenausschreibungid;
  }

  public void setStellenausschreibungid(Long stellenausschreibungid) {
    this.stellenausschreibungid = stellenausschreibungid;
  }

  @Override
  public String toString() {
    return "Bewerbung{"
        + "bewerbungsid="
        + bewerbungsid
        + ", stellenausschreibung="
        + stellenausschreibungid.toString()
        + ", anschreiben='"
        + anschreiben
        + '\''
        + ", lebenslauf='"
        + lebenslauf
        + '\''
        + ", referenzen='"
        + referenzen
        + '\''
        + '}';
  }
}
