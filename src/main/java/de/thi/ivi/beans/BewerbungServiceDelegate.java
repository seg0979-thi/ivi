package de.thi.ivi.beans;

import de.thi.ivi.LoggerDelegate;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.camunda.bpm.engine.delegate.DelegateExecution;

/** @author tog8596 Tobias Gaul */
@Stateless
@LocalBean
@Named
public class BewerbungServiceDelegate implements BewerbungServiceDelegateLocal {

  @Inject BewerbungServiceBean bewerbungService;

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public BewerbungServiceDelegate() {}

  public void createBewerbung(DelegateExecution execution) {
    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");

    Bewerbung bewerbung =
        new Bewerbung(
            (String) execution.getVariable("anschreiben"),
            (String) execution.getVariable("lebenslauf"),
            (String) execution.getVariable("referenzen"),
            stellenausschreibung.getStellenausschreibungid());

    LOGGER.info("createBewerbung: " + bewerbung);

    Bewerbung bewerbungResult = bewerbungService.create(bewerbung);
    execution.setVariable("bewerbung", bewerbungResult);
    LOGGER.info("createBewerbung after DB create: " + bewerbungResult);
  }

  public void readBewerbung(DelegateExecution execution) {
    String bewerbungsid = (String) execution.getVariable("bewerbungsid");
    LOGGER.info("readBewerbung: " + bewerbungsid);
    Bewerbung bewerbung = bewerbungService.read(Long.valueOf(bewerbungsid));
    LOGGER.info("readBewerbung after DB: " + bewerbung);
    execution.setVariable("bewerbung", bewerbung);
  }

  public void updateBewerbung(DelegateExecution execution) {
    Bewerbung bewerbung = (Bewerbung) execution.getVariable("bewerbung");
    LOGGER.info("updateBewerbung: " + bewerbung);
    bewerbungService.update(bewerbung);
  }

  public void deleteBewerbung(DelegateExecution execution) {
    Bewerbung bewerbung = (Bewerbung) execution.getVariable("bewerbung");
    LOGGER.info("deleteBewerbung: " + bewerbung);
    bewerbungService.update(bewerbung);
  }
}
