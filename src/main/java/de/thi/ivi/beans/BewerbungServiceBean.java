package de.thi.ivi.beans;

import de.thi.ivi.LoggerDelegate;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/** @author tog8596 Tobias Gaul */
@Stateless
@LocalBean
public class BewerbungServiceBean implements BewerbungServiceBeanRemote, BewerbungServiceBeanLocal {
  @PersistenceContext(unitName = "ivi-PU")
  EntityManager em;

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public BewerbungServiceBean() {}

  public Bewerbung create(Bewerbung bewerbung) {
    LOGGER.info("Try to create Bewerbung" + bewerbung);
    em.persist(bewerbung);
    return bewerbung;
  }

  public Bewerbung read(Long id) {
    LOGGER.info("Try to read Bewerbung by id: " + id);
    return this.em.find(Bewerbung.class, id);
  }

  public void update(Bewerbung bewerbung) {
    LOGGER.info("Try to merge Bewerbung: " + bewerbung);
    this.em.merge(bewerbung);
  }

  public void delete(Bewerbung bewerbung) {
    Bewerbung bewerbungToDelete = read(bewerbung.getBewerbungsid());
    if (bewerbungToDelete != null) {
      LOGGER.info("Try to delete Bewerbung: " + bewerbungToDelete);
      this.em.remove(bewerbung);
    }
  }
}
