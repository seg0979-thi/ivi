package de.thi.ivi.beans;

/** @author seg0979 Sebastian Gaiser */
public enum StellenauschreibungStati {
  NICHT_AUSGESCHRIEBEN("nicht ausgeschrieben"),
  AUSGESCHRIEBEN("ausgeschrieben"),
  GESCHLOSSEN("geschlossen");

  String status;

  StellenauschreibungStati(String status) {
    this.status = status;
  }
}
