package de.thi.ivi.beans;

import de.thi.ivi.LoggerDelegate;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/** @author seg0979 Sebastian Gaiser */
@Stateless
@LocalBean
public class StellenausschreibungServiceBean
    implements StellenausschreibungServiceBeanRemote, StellenausschreibungServiceBeanLocal {
  @PersistenceContext(unitName = "ivi-PU")
  EntityManager em;

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public StellenausschreibungServiceBean() {}

  public Stellenausschreibung create(Stellenausschreibung stellenausschreibung) {
    LOGGER.info("Try to create Stellenausschreibung" + stellenausschreibung);
    em.persist(stellenausschreibung);
    return stellenausschreibung;
  }

  public Stellenausschreibung read(Long id) {
    LOGGER.info("Try to read Stellenausschreibung by id: " + id);
    return this.em.find(Stellenausschreibung.class, id);
  }

  public void update(Stellenausschreibung stellenausschreibung) {
    LOGGER.info("Try to merge Stellenausschreibung: " + stellenausschreibung);
    this.em.merge(stellenausschreibung);
  }

  public void delete(Stellenausschreibung stellenausschreibung) {
    Stellenausschreibung stellenausschreibungToDelete =
        read(stellenausschreibung.getStellenausschreibungid());
    if (stellenausschreibungToDelete != null) {
      LOGGER.info("Try to delete Stellenausschreibung: " + stellenausschreibungToDelete);
      this.em.remove(stellenausschreibung);
    }
  }
}
