package de.thi.ivi.beans;

import static de.thi.ivi.beans.StellenauschreibungStati.NICHT_AUSGESCHRIEBEN;

import de.thi.ivi.LoggerDelegate;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.camunda.bpm.engine.delegate.DelegateExecution;

/** @author seg0979 Sebastian Gaiser */
@Stateless
@LocalBean
@Named
public class StellenausschreibungServiceDelegate
    implements StellenausschreibungServiceDelegateLocal {

  @Inject StellenausschreibungServiceBean stellenausschreibungService;

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public StellenausschreibungServiceDelegate() {}

  public void createStellenausschreibung(DelegateExecution execution) {
    Stellenausschreibung stellenausschreibung =
        new Stellenausschreibung(
            (String) execution.getVariable("Stellenname"),
            (String) execution.getVariable("Stellenbeschreibung"),
            NICHT_AUSGESCHRIEBEN.toString(),
            (String) execution.getVariable("dringlichkeit"),
            null,
            execution.getProcessInstanceId());

    LOGGER.info("createStellenausschreibung: " + stellenausschreibung);

    Stellenausschreibung stellenausschreibungResult =
        stellenausschreibungService.create(stellenausschreibung);
    execution.setVariable("stellenausschreibung", stellenausschreibungResult);
    LOGGER.info("createStellenausschreibung after DB create: " + stellenausschreibungResult);
  }

  public void readStellenauschreibung(DelegateExecution execution) {
    String stellenausschreibungId = (String) execution.getVariable("stellenausschreibungId");
    LOGGER.info("readStellenausschreibung: " + stellenausschreibungId);
    Stellenausschreibung stellenausschreibung =
        stellenausschreibungService.read(Long.valueOf(stellenausschreibungId));
    LOGGER.info("readStellenausschreibung after DB: " + stellenausschreibung);
    execution.setVariable("stellenausschreibung", stellenausschreibung);
  }

  public void updateStellenauschreibung(DelegateExecution execution) {
    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");
    LOGGER.info("updateStellenausschreibung: " + stellenausschreibung);
    stellenausschreibungService.update(stellenausschreibung);
  }

  public void deleteStellenauschreibung(DelegateExecution execution) {
    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");
    LOGGER.info("deleteStellenausschreibung: " + stellenausschreibung);
    stellenausschreibungService.update(stellenausschreibung);
  }
}
