package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.messaging.SetZusage;
import de.thi.ivi.services.model.Zusage;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author tog8596 Tobias Gaul */
public class ZusageSenden implements JavaDelegate {

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("Zusage wird gesetzt");
    Zusage zusage = new Zusage("Wir haben uns für Sie entschieden!");
    LOGGER.info(zusage.toString());
    SetZusage.storeZusage(zusage);
  }
}
