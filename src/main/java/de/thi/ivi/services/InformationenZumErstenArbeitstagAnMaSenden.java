package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.messaging.SendInfosErsterArbeitstag;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
public class InformationenZumErstenArbeitstagAnMaSenden implements JavaDelegate {

  private static final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("Informationen zum ersten Arbeitstag an Mitarbeiter senden");
    String infos = (String) execution.getVariable("infosErsterArbeitstag");
    LOGGER.info("InfosErsterArbeitstag: " + infos);
    SendInfosErsterArbeitstag.sendInfosErsterArbeitstag(infos);
  }
}
