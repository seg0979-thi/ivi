package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/** @author seg0979 Sebastian Gaiser */
/** @author ane3106 Andrej Elokhov */
/** @author tog8596 Tobias Gaul */
public class BewerberAngenommen implements JavaDelegate {
  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");
    LOGGER.info(
        "BewerberAngenommen, sende an Einstellungsprozess mit ProcessInstanceId: "
            + stellenausschreibung.getProcessInstanceId());

    execution
        .getProcessEngineServices()
        .getRuntimeService()
        .createMessageCorrelation("Mitarbeiter")
        .processInstanceId(stellenausschreibung.getProcessInstanceId())
        .correlate();
  }
}
