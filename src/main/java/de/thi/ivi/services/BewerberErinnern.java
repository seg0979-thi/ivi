package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.messaging.SetTermin;
import de.thi.ivi.services.model.Termin;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author tog8596 Tobias Gaul */
public class BewerberErinnern implements JavaDelegate {

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    // Korrelation für nachfolgenden Empfang
    execution.setVariable("einladungAkzeptiert", false);
    execution
        .getProcessEngineServices()
        .getRuntimeService()
        .createMessageCorrelation("Antwort")
        .setVariable("einladungAkzeptiert", false)
        .correlateAll();

    // Queue
    LOGGER.info("Termin wird gesetzt");
    String eigenschaften = (String) execution.getVariable("termin");
    Termin termin =
        new Termin(
            "Bewerber", "Hiermit erinnern wir Sie an den folgenden Termin: " + eigenschaften);
    LOGGER.info(termin.toString());
    SetTermin.storeTermin(termin);
  }
}
