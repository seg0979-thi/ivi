package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import de.thi.ivi.messaging.SetStellenausschreibung;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
public class StelleAusschreiben implements JavaDelegate {
  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("StelleAusschreiben");

    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");

    LOGGER.info("StelleAusschreiben PID: " + execution.getProcessInstanceId());
    stellenausschreibung.setProcessInstanceId(execution.getProcessInstanceId());

    // Will be set via Camel Processor and gets replied by Aggregator
    execution.setVariable("stellenausschreibungwebsiteid", "nicht gesetzt");
    execution
        .getProcessEngineServices()
        .getRuntimeService()
        .createMessageCorrelation("Bestaetigung")
        .setVariable("stellenausschreibungwebsiteid", "nicht gesetzt")
        .correlateAll();

    SetStellenausschreibung.createStellenausschreibung(stellenausschreibung);
  }
}
