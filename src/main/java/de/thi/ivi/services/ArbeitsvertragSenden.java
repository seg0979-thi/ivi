package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.messaging.SetArbeitsvertrag;
import de.thi.ivi.services.model.Arbeitsvertrag;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
/** @author ane3105 Andrej Elokhov */
public class ArbeitsvertragSenden implements JavaDelegate {
  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {

    try {
      // Queue
      LOGGER.info("Arbeitsvertrag wird gesendet");
      Arbeitsvertrag arbeitsvertrag = (Arbeitsvertrag) execution.getVariable("arbeitsvertrag");

      LOGGER.info(arbeitsvertrag.toString());
      SetArbeitsvertrag.storeArbeitsvertrag(arbeitsvertrag);
    } catch (Exception e) {
      LOGGER.info("Exception occured in Service \"ArbeitsvertragSenden\": " + e.getMessage());
      e.printStackTrace();
    }
  }
}
