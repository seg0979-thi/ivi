package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import de.thi.ivi.services.model.Arbeitsvertrag;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
/** @author ane3106 Andrej Elokhov */
public class ArbeitsvertragAufsetzen implements JavaDelegate {
  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {

    LOGGER.info("Service \"ArbeitsvertragAufsetzen\" started...");

    try {
      String stellenName =
          ((Stellenausschreibung) execution.getVariable("stellenausschreibung"))
              .getStellenausschreibungname();

      String vorname = (String) execution.getVariable("bewerberVorname");
      String nachname = (String) execution.getVariable("bewerberNachname");
      String eintrittsDatum = (String) execution.getVariable("eintrittsDatum");
      String austrittsDatum = (String) execution.getVariable("austrittsDatum");
      long jahresgehalt = (long) execution.getVariable("jahresGehalt");

      Arbeitsvertrag arbeitsvertrag =
          new Arbeitsvertrag(
              vorname, nachname, jahresgehalt, eintrittsDatum, austrittsDatum, stellenName);

      execution.setVariable("arbeitsvertrag", arbeitsvertrag);

    } catch (Exception e) {
      LOGGER.info(
          "\"ArbeitsvertragAufsetzen\"| Exception occured while reading p.-execution: "
              + e.getMessage());
    }
  }
}
