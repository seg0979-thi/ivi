package de.thi.ivi.services;

import static de.thi.ivi.beans.StellenauschreibungStati.AUSGESCHRIEBEN;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
public class StatusDerStellenausschreibungPruefen implements JavaDelegate {
  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("Status des Stellenausschreibung pruefen");

    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");
    LOGGER.info("Status des Stellenausschreibung pruefen: " + stellenausschreibung);

    if (stellenausschreibung.getStellenausschreibungstatus().equals(AUSGESCHRIEBEN.toString())) {
      execution.setVariable("stellenausschreibungOffen", true);
    } else {
      execution.setVariable("stellenausschreibungOffen", false);
    }
  }
}
