package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.messaging.SetAbsage;
import de.thi.ivi.services.model.Absage;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author tog8596 Tobias Gaul */
public class StelleNichtMehrAusgeschrieben implements JavaDelegate {

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("Absage wird gesetzt");
    Absage absage =
        new Absage("Sehr geehrtes Individuum, leider ist diese Stelle bereits besetzt!", null);
    LOGGER.info(absage.toString());
    SetAbsage.storeAbsage(absage);
  }
}
