package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.messaging.SetTermin;
import de.thi.ivi.services.model.Termin;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author tog8596 Tobias Gaul */
public class AnstehendeVereinbarungInformieren implements JavaDelegate {

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    // Korrelation für nachfolgenden Empfang
    execution.setVariable("bewerberAkzeptiert ", false);
    execution.setVariable("termin", "01.01.1970");
    execution
        .getProcessEngineServices()
        .getRuntimeService()
        .createMessageCorrelation("Termin")
        .setVariable("termin", "01.01.1970")
        .correlateAll();

    // Queue
    LOGGER.info("Termin wird gesetzt");
    Termin termin =
        new Termin("Recruiter / Führungskraft / Teamleiter", "Bitte planen Sie einen Termin");
    LOGGER.info(termin.toString());
    SetTermin.storeTermin(termin);
  }
}
