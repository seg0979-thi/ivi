package de.thi.ivi.services;

import de.thi.ivi.services.model.Verbesserungsvorschlag;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author seg0979 Sebastian Gaiser
 * @author tog8596 Tobias Gaul
 */
public class BewerbungVollstaendigkeitPruefen implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    String anschreiben = (String) execution.getVariable("anschreiben");
    String lebenslauf = (String) execution.getVariable("lebenslauf");
    String referenzen = (String) execution.getVariable("referenzen");

    Verbesserungsvorschlag verbesserungsvorschlag = new Verbesserungsvorschlag();
    if (anschreiben != null
        && lebenslauf != null
        && referenzen != null
        && !anschreiben.equals("")
        && !lebenslauf.equals("")
        && !referenzen.equals("")) {
      execution.setVariable("verbesserungsvorschlag", verbesserungsvorschlag);
      execution.setVariable(
          "bewerbungsunterlagenVollstaendig", true); // Unterlagen sind vollständig
    } else {
      String vollstaendigkeit =
          "Ihre Unterlagen sind unvollständig, bitte achten Sie darauf uns ein Anschreiben, Ihren"
              + " Lebenslauf und Ihre Referenzen zuzusenden!";
      verbesserungsvorschlag.setVollstaendigkeit(
          vollstaendigkeit); // Anmerkung zur Vollständigkeit setzen

      execution.setVariable("verbesserungsvorschlag", verbesserungsvorschlag);
      execution.setVariable(
          "bewerbungsunterlagenVollstaendig", false); // Unterlagen sind nicht vollständig
    }

    // Wenn der Prozess durch das Signal abgebrochen wird, kommt der Fehler "Unknown
    // property used
    // in expression", wenn wir die Variablen nicht zuvor setzen.
    execution.setVariable("anforderungenErfuellt", false);
    execution.setVariable("qualifikationenZufriedenstellend", false);
  }
}
