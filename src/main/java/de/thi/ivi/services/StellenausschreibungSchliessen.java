package de.thi.ivi.services;

import static de.thi.ivi.beans.StellenauschreibungStati.GESCHLOSSEN;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import de.thi.ivi.rest.StelleAufWebsiteSchliessen;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
public class StellenausschreibungSchliessen implements JavaDelegate {

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("StellenausschreibungSchliessen");

    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");
    LOGGER.info("StellenausschreibungSchliessen before: " + stellenausschreibung.toString());

    stellenausschreibung.setStellenausschreibungstatus(GESCHLOSSEN.toString());

    String websiteId = StelleAufWebsiteSchliessen.stelleAufWebsiteSchliessen(stellenausschreibung);
    stellenausschreibung.setStellenausschreibungwebsiteid(websiteId);

    LOGGER.info("StellenausschreibungSchliessen after: " + stellenausschreibung);

    execution.setVariable("stellenausschreibung", stellenausschreibung);
  }
}
