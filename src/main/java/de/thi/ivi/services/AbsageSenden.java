package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.messaging.SetAbsage;
import de.thi.ivi.services.model.Absage;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author tog8596 Tobias Gaul */
public class AbsageSenden implements JavaDelegate {

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("Absage wird gesetzt");
    Absage absage =
        new Absage(
            "Sehr geehrtes Individuum, wir beziehen uns auf Ihre Bewerbung und bedanken uns hiermit"
                + " nochmals für Ihr Interesse an einer Mitarbeit in unserem Hause. Nach"
                + " sorgfältiger Prüfung Ihrer Bewerbungsunterlagen müssen wir Ihnen leider"
                + " mitteilen, dass wir uns nicht für Sie entschieden haben. Wir bedauern, dass wir"
                + " Sie nicht berücksichtigen konnten.",
            null);
    LOGGER.info(absage.toString());
    SetAbsage.storeAbsage(absage);
  }
}
