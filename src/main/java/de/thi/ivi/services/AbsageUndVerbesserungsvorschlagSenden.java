package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.messaging.SetAbsage;
import de.thi.ivi.services.model.Absage;
import de.thi.ivi.services.model.Verbesserungsvorschlag;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author tog8596 Tobias Gaul */
public class AbsageUndVerbesserungsvorschlagSenden implements JavaDelegate {

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    Verbesserungsvorschlag verbesserungsvorschlag =
        (Verbesserungsvorschlag) execution.getVariable("verbesserungsvorschlag");
    if (verbesserungsvorschlag.getVollstaendigkeit() == null
        || verbesserungsvorschlag.getVollstaendigkeit().equals("")) {
      String verbesserungsvorschlaganforderungen =
          (String) execution.getVariable("verbesserungsvorschlaganforderungen");
      if (!verbesserungsvorschlaganforderungen.equals("")) {
        verbesserungsvorschlag.setAnforderungen(verbesserungsvorschlaganforderungen);
      }
    }
    if (verbesserungsvorschlag.getAnforderungen() == null
        || verbesserungsvorschlag.getAnforderungen().equals("")) {
      String verbesserungsvorschlagqualifikationen =
          (String) execution.getVariable("verbesserungsvorschlagqualifikationen");
      verbesserungsvorschlag.setQualifikationen(verbesserungsvorschlagqualifikationen);
    }

    LOGGER.info("Absage wird gesetzt");
    Absage absage =
        new Absage(
            "Sehr geehrtes Individuum, wir beziehen uns auf Ihre Bewerbung und bedanken uns hiermit"
                + " nochmals für Ihr Interesse an einer Mitarbeit in unserem Hause. Nach"
                + " sorgfältiger Prüfung Ihrer Bewerbungsunterlagen müssen wir Ihnen leider"
                + " mitteilen, dass wir uns nicht für Sie entschieden haben. Wir bedauern, dass wir"
                + " Sie nicht berücksichtigen konnten.",
            verbesserungsvorschlag);

    LOGGER.info("VERBESSERUNG: " + verbesserungsvorschlag.toString());

    LOGGER.info(absage.toString());
    SetAbsage.storeAbsage(absage);
  }
}
