package de.thi.ivi.services;

import static de.thi.ivi.beans.StellenauschreibungStati.NICHT_AUSGESCHRIEBEN;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import de.thi.ivi.beans.StellenausschreibungServiceBean;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
public class StelleSpeichern implements JavaDelegate {

  @Inject private StellenausschreibungServiceBean stellenausschreibungServiceBean;

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) {
    LOGGER.info("StelleSpeichern.execute()");

    Stellenausschreibung stellenausschreibung =
        new Stellenausschreibung(
            (String) execution.getVariable("Stellenname"),
            (String) execution.getVariable("Stellenbeschreibung"),
            NICHT_AUSGESCHRIEBEN.toString(),
            (String) execution.getVariable("dringlichkeit"),
            "nicht gesetzt",
            execution.getProcessInstanceId());

    execution.setVariable("stellenausschreibung", stellenausschreibung);

    stellenausschreibungServiceBean.create(stellenausschreibung);
  }
}
