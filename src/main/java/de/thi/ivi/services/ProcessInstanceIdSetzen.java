package de.thi.ivi.services;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
public class ProcessInstanceIdSetzen implements JavaDelegate {
  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("ProcessInstanceIdSetzen");
    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");

    LOGGER.info("StelleAusschreiben PID old: " + stellenausschreibung.getProcessInstanceId());
    LOGGER.info("StelleAusschreiben PID new: " + execution.getProcessInstanceId());
    stellenausschreibung.setProcessInstanceId(execution.getProcessInstanceId());
  }
}
