package de.thi.ivi.services;

import static de.thi.ivi.beans.StellenauschreibungStati.AUSGESCHRIEBEN;

import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/** @author seg0979 Sebastian Gaiser */
public class StatusDerStellenausschreibungAufAusgeschriebenSetzen implements JavaDelegate {

  private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("StatusDerStellenausschreibungAufAusgeschriebenSetzen");

    LOGGER.info(
        "stellenausschreibungwebsiteid: " + execution.getVariable("stellenausschreibungwebsiteid"));

    Stellenausschreibung stellenausschreibung =
        (Stellenausschreibung) execution.getVariable("stellenausschreibung");
    LOGGER.info(
        "StatusDerStellenausschreibungAufAusgeschriebenSetzen before: " + stellenausschreibung);

    stellenausschreibung.setStellenausschreibungstatus(AUSGESCHRIEBEN.toString());
    stellenausschreibung.setStellenausschreibungwebsiteid(
        (String) execution.getVariable("stellenausschreibungwebsiteid"));
    LOGGER.info(
        "StatusDerStellenausschreibungAufAusgeschriebenSetzen after: " + stellenausschreibung);

    execution.setVariable("stellenausschreibung", stellenausschreibung);
  }
}
