package de.thi.ivi.services.model;

import java.io.Serializable;

/** @author tog8596 Tobias Gaul */
public class Termin implements Serializable {

  private static final long serialVersionUID = 1L;
  private String empfaenger;
  private String termin;

  public Termin(String empfaenger, String termin) {
    this.empfaenger = empfaenger;
    this.termin = termin;
  }

  public String getEmpfaenger() {
    return empfaenger;
  }

  public void setEmpfaenger(String empfaenger) {
    this.empfaenger = empfaenger;
  }

  public String getTermin() {
    return termin;
  }

  public void setTermin(String termin) {
    this.termin = termin;
  }

  @Override
  public String toString() {
    return "Termin [empfaenger=" + empfaenger + ", termin=" + termin + "]";
  }
}
