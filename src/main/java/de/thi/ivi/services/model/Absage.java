package de.thi.ivi.services.model;

import java.io.Serializable;

/** @author tog8596 Tobias Gaul */
public class Absage implements Serializable {

  private static final long serialVersionUID = 1L;
  private String nachricht;
  private Verbesserungsvorschlag verbesserungsvorschlag;

  public Absage() {
    this.nachricht = null;
    this.verbesserungsvorschlag = null;
  }

  public Absage(String nachricht, Verbesserungsvorschlag verbesserungsvorschlag) {
    this.nachricht = nachricht;
    this.verbesserungsvorschlag = verbesserungsvorschlag;
  }

  public String getNachricht() {
    return nachricht;
  }

  public void setNachricht(String nachricht) {
    this.nachricht = nachricht;
  }

  public Verbesserungsvorschlag getVerbesserungsvorschlag() {
    return verbesserungsvorschlag;
  }

  public void setVerbesserungsvorschlag(Verbesserungsvorschlag verbesserungsvorschlag) {
    this.verbesserungsvorschlag = verbesserungsvorschlag;
  }

  @Override
  public String toString() {
    if (this.verbesserungsvorschlag != null) {
      return "Absage [nachricht="
          + nachricht
          + ", verbesserungsvorschlag="
          + verbesserungsvorschlag.toString()
          + "]";
    } else {
      return "Absage [nachricht=" + nachricht + "]";
    }
  }
}
