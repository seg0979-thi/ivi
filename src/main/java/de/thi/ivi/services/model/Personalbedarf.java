package de.thi.ivi.services.model;

import java.io.Serializable;
import java.util.UUID;

/** @author seg0979 Sebastian Gaiser */
public class Personalbedarf implements Serializable {
  private static final long serialVersionUID = 1L;

  private UUID personalbedarfId;
  private Stelle stelle;
  private String status;
  private String dringlichkeit;

  public Personalbedarf() {
    this.personalbedarfId = null;
    this.stelle = null;
    this.status = null;
    this.dringlichkeit = null;
  }

  public Personalbedarf(Stelle stelle, String status, String dringlichkeit) {
    this.personalbedarfId = UUID.randomUUID();
    this.stelle = stelle;
    this.status = status;
    this.dringlichkeit = dringlichkeit;
  }

  public UUID getPersonalbedarfId() {
    return personalbedarfId;
  }

  public void setPersonalbedarfId(UUID personalbedarfId) {
    this.personalbedarfId = personalbedarfId;
  }

  public Stelle getStelle() {
    return stelle;
  }

  public void setStelle(Stelle stelle) {
    this.stelle = stelle;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDringlichkeit() {
    return dringlichkeit;
  }

  public void setDringlichkeit(String dringlichkeit) {
    this.dringlichkeit = dringlichkeit;
  }

  @Override
  public String toString() {
    return "Personalbedarf [personalbedarfId="
        + personalbedarfId
        + ", stelle="
        + stelle
        + ", status="
        + status
        + ", dringlichkeit="
        + dringlichkeit
        + "]";
  }
}
