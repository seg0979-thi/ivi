package de.thi.ivi.services.model;

import java.io.Serializable;

/** @author tog8596 Tobias Gaul */
public class Zusage implements Serializable {

  private static final long serialVersionUID = 1L;
  private String nachricht;

  public Zusage() {
    this.nachricht = null;
  }

  public Zusage(String nachricht) {
    this.nachricht = nachricht;
  }

  public String getNachricht() {
    return nachricht;
  }

  public void setNachricht(String nachricht) {
    this.nachricht = nachricht;
  }

  @Override
  public String toString() {
    return "Zusage [nachricht=" + nachricht + "]";
  }
}
