package de.thi.ivi.services.model;

import java.io.Serializable;
import java.util.UUID;

/** @author seg0979 Sebastian Gaiser */
public class Stelle implements Serializable {
  private static final long serialVersionUID = 1L;

  private UUID stellenId;
  private String namen;
  private String beschreibung;

  public Stelle() {
    this.stellenId = null;
    this.namen = null;
    this.beschreibung = null;
  }

  public Stelle(String namen, String beschreibung) {
    this.stellenId = UUID.randomUUID();
    this.namen = namen;
    this.beschreibung = beschreibung;
  }

  public UUID getStellenId() {
    return stellenId;
  }

  public void setStellenId(UUID stellenId) {
    this.stellenId = stellenId;
  }

  public String getNamen() {
    return namen;
  }

  public void setNamen(String namen) {
    this.namen = namen;
  }

  public String getBeschreibung() {
    return beschreibung;
  }

  public void setBeschreibung(String beschreibung) {
    this.beschreibung = beschreibung;
  }

  @Override
  public String toString() {
    return "Stelle [stellenId="
        + stellenId
        + ", namen="
        + namen
        + ", beschreibung="
        + beschreibung
        + "]";
  }
}
