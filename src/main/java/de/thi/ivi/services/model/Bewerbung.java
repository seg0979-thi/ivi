package de.thi.ivi.services.model;

import java.io.Serializable;

// TODO move Bewerbung beans package
/** @author seg0979 Sebastian Gaiser */
public class Bewerbung implements Serializable {
  // TODO fix UUID handling
  //  private static long serialVersionUID = 1L;
  // Namen, EMail, Tel., Adresse strings
  // anschreiben lebenslauf referenzen dateien
  private String anschreiben;
  private String lebenslauf;
  private String referenzen;

  public Bewerbung() {
    //    this.serialVersionUID = 0;
    this.anschreiben = null;
    this.lebenslauf = null;
    this.referenzen = null;
  }

  public Bewerbung(String anschreiben, String lebenslauf, String referenzen) {
    //    this.serialVersionUID = (int) Math.ceil(Math.random() * 10000);
    this.anschreiben = anschreiben;
    this.lebenslauf = lebenslauf;
    this.referenzen = referenzen;
  }

  public String getAnschreiben() {
    return anschreiben;
  }

  public void setAnschreiben(String anschreiben) {
    this.anschreiben = anschreiben;
  }

  public String getLebenslauf() {
    return lebenslauf;
  }

  public void setLebenslauf(String lebenslauf) {
    this.lebenslauf = lebenslauf;
  }

  public String getReferenzen() {
    return referenzen;
  }

  public void setReferenzen(String referenzen) {
    this.referenzen = referenzen;
  }

  @Override
  public String toString() {
    return "Bewerbung{"
        + "anschreiben='"
        + anschreiben
        + '\''
        + ", lebenslauf='"
        + lebenslauf
        + '\''
        + ", referenzen='"
        + referenzen
        + '\''
        + '}';
  }
}
