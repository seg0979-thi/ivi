package de.thi.ivi.services.model;

import java.io.Serializable;

/** @author tog8596 Tobias Gaul */
public class Verbesserungsvorschlag implements Serializable {
  private static final long serialVersionUID = 1L;
  private String vollstaendigkeit;
  private String anforderungen;
  private String qualifikationen;

  public Verbesserungsvorschlag() {
    // this.serialVersionUID = 0;
    this.vollstaendigkeit = null;
    this.anforderungen = null;
    this.qualifikationen = null;
  }

  public Verbesserungsvorschlag(
      String vollständigkeit, String anforderungen, String qualifikationen) {
    this.vollstaendigkeit = vollständigkeit;
    this.anforderungen = anforderungen;
    this.qualifikationen = qualifikationen;
  }

  public String getVollstaendigkeit() {
    return vollstaendigkeit;
  }

  public void setVollstaendigkeit(String vollstaendigkeit) {
    this.vollstaendigkeit = vollstaendigkeit;
  }

  public String getAnforderungen() {
    return anforderungen;
  }

  public void setAnforderungen(String anforderungen) {
    this.anforderungen = anforderungen;
  }

  public String getQualifikationen() {
    return qualifikationen;
  }

  public void setQualifikationen(String qualifikationen) {
    this.qualifikationen = qualifikationen;
  }

  @Override
  public String toString() {
    return "Verbesserungsvorschlag [vollstaendigkeit="
        + vollstaendigkeit
        + ", anforderungen="
        + anforderungen
        + ", qualifikationen="
        + qualifikationen
        + "]";
  }
}
