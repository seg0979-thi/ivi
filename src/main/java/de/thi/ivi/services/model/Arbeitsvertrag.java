package de.thi.ivi.services.model;

import java.io.Serializable;

/** @author ane3106 Andrej Elokhov */
public class Arbeitsvertrag implements Serializable {

  private static final long serialVersionUID = 1L;

  private String bewerberVorname;
  private String bewerberNachname;
  private long jahresGehalt;
  private String eintrittsDatum;
  private String austrittsDatum;
  private String stellenName;

  public Arbeitsvertrag() {
    bewerberVorname = "";
    bewerberNachname = "";
    jahresGehalt = 0;
    eintrittsDatum = "";
    austrittsDatum = "";
    stellenName = "";
  }

  public Arbeitsvertrag(
      String vorname,
      String nachname,
      long gehalt,
      String eintritt,
      String austritt,
      String stellenname) {
    this.bewerberNachname = nachname;
    this.bewerberVorname = vorname;
    this.eintrittsDatum = eintritt;
    this.austrittsDatum = austritt;
    this.jahresGehalt = gehalt;
    this.setStellenName(stellenname);
  }

  public String getBewerberVorname() {
    return bewerberVorname;
  }

  public void setBewerberVorname(String bewerberVorname) {
    this.bewerberVorname = bewerberVorname;
  }

  public String getBewerberNachname() {
    return bewerberNachname;
  }

  public void setBewerberNachname(String bewerberNachname) {
    this.bewerberNachname = bewerberNachname;
  }

  public String getEintrittsDatum() {
    return eintrittsDatum;
  }

  public void setEintrittsDatum(String eintrittsDatum) {
    this.eintrittsDatum = eintrittsDatum;
  }

  public String getAustrittsDatum() {
    return austrittsDatum;
  }

  public void setAustrittsDatum(String austrittsDatum) {
    this.austrittsDatum = austrittsDatum;
  }

  public long getJahresGehalt() {
    return jahresGehalt;
  }

  public void setJahresGehalt(long jahresGehalt) {
    this.jahresGehalt = jahresGehalt;
  }

  public String getStellenName() {
    return stellenName;
  }

  public void setStellenName(String stellenName) {
    this.stellenName = stellenName;
  }

  @Override
  public String toString() {
    return "Arbeitsvertrag [bewerberVorname="
        + bewerberVorname
        + ", bewerberNachname="
        + bewerberNachname
        + ", eintrittsDatum="
        + eintrittsDatum
        + ", austrittsDatum="
        + austrittsDatum
        + ", jahresGehalt="
        + jahresGehalt
        + "]";
  }
}
