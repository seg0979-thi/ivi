package de.thi.ivi;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

/** @author seg0979 Sebastian Gaiser */
@ProcessApplication
public class CamundaBpmProcessApplication extends ServletProcessApplication {

  private static final String PROCESS_DEFINITION_KEY = "ivi";
  public static final String BROKER_URL = "tcp://messaging:61616";

  public static final String MOCK_URL = "http://mock:8090";
  public static final String AUSSCHREIBEN = "/stelle";
  public static final String SCHLIESSEN = AUSSCHREIBEN + "/close";
}
