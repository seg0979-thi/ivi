package de.thi.ivi.rest;

import com.google.gson.Gson;
import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import de.thi.ivi.services.model.WebsiteResponse;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

/** @author seg0979 Sebastian Gaiser */
public class StelleAufWebsiteSchliessen {
  private static final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public static String stelleAufWebsiteSchliessen(Stellenausschreibung stellenausschreibung) {
    JSONObject json = new JSONObject();
    json.put("websiteuuid", stellenausschreibung.getStellenausschreibungwebsiteid());

    try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
      HttpPost post = new HttpPost("http://mock:8090/stelle/close");
      StringEntity params = new StringEntity(json.toString());

      post.addHeader("Content-type", "application/json");
      post.setEntity(params);

      HttpResponse response = httpClient.execute(post);
      LOGGER.info("Response Code: " + response.getStatusLine().getStatusCode());

      String result = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
      LOGGER.info("Response Content: " + result);
      response.getEntity().getContent().close();

      WebsiteResponse websiteResponse = new Gson().fromJson(result, WebsiteResponse.class);
      LOGGER.info("Website Response: " + websiteResponse.toString());
      return websiteResponse.getUuid().toString();
    } catch (Exception ex) {
      LOGGER.info("Exception : " + ex);
      throw new RuntimeException(ex);
    }
  }
}
