package de.thi.ivi.messaging;

import static de.thi.ivi.messaging.JmsRoutes.INFOS_ERSTER_ARBEITSTAG_QUEUE_NAME;

import com.google.gson.Gson;
import de.thi.ivi.LoggerDelegate;
import java.util.logging.Logger;
import javax.jms.*;

/** @author seg0979 Sebastian Gaiser */
public class SendInfosErsterArbeitstag {
  private static final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public static void sendInfosErsterArbeitstag(String infos) {
    try {
      Connection connection = JmsSessionFactory.createJmsConnection();
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

      Destination destination = session.createQueue(INFOS_ERSTER_ARBEITSTAG_QUEUE_NAME);
      MessageProducer producer = session.createProducer(destination);
      producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

      TextMessage message = session.createTextMessage(new Gson().toJson(infos));
      LOGGER.info("Sending message: " + message.getText());
      producer.send(message);
      connection.close();

    } catch (JMSException e) {
      LOGGER.info(e.getMessage());
    }
  }
}
