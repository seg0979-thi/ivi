package de.thi.ivi.messaging;

import static de.thi.ivi.CamundaBpmProcessApplication.BROKER_URL;

import javax.jms.Connection;
import javax.jms.JMSException;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

/** @author seg0979 Sebastian Gaiser */
public class JmsSessionFactory {

  public static Connection createJmsConnection() throws JMSException {
    String user = ActiveMQConnection.DEFAULT_USER;
    String password = ActiveMQConnection.DEFAULT_PASSWORD;

    ActiveMQConnectionFactory connectionFactory =
        new ActiveMQConnectionFactory(user, password, BROKER_URL);
    Connection connection = connectionFactory.createConnection();
    connection.start();

    return connection;
  }
}
