package de.thi.ivi.messaging;

import static de.thi.ivi.messaging.JmsRoutes.ARBEITSVERTRAG_QUEUE_NAME;

import com.google.gson.Gson;
import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.services.model.Arbeitsvertrag;
import java.util.logging.Logger;
import javax.jms.*;

/** @author ane3106 Andrej Elokhov */
public class SetArbeitsvertrag {

  private static final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public static void storeArbeitsvertrag(Arbeitsvertrag arbeitsvertrag) {
    try {
      Connection connection = JmsSessionFactory.createJmsConnection();
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

      Destination destination = session.createQueue(ARBEITSVERTRAG_QUEUE_NAME);
      MessageProducer producer = session.createProducer(destination);
      producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

      TextMessage message = session.createTextMessage(new Gson().toJson(arbeitsvertrag));
      LOGGER.info("Sending message: " + message.getText());
      producer.send(message);
      connection.close();

    } catch (JMSException e) {
      LOGGER.info(e.getMessage());
    }
  }
}
