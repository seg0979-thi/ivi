package de.thi.ivi.messaging;

import static de.thi.ivi.messaging.JmsRoutes.ZUSAGE_QUEUE_NAME;

import com.google.gson.Gson;
import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.services.model.Zusage;
import java.util.logging.Logger;
import javax.jms.*;

/** @author tog8596 Tobias Gaul */
public class SetZusage {
  private static final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public static void storeZusage(Zusage zusage) {
    try {
      Connection connection = JmsSessionFactory.createJmsConnection();
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

      Destination destination = session.createQueue(ZUSAGE_QUEUE_NAME);
      MessageProducer producer = session.createProducer(destination);
      producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

      TextMessage message = session.createTextMessage(new Gson().toJson(zusage));
      LOGGER.info("Sending message: " + message.getText());
      producer.send(message);
      connection.close();

    } catch (JMSException e) {
      LOGGER.info(e.getMessage());
    }
  }
}
