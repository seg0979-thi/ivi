package de.thi.ivi.messaging;

import static de.thi.ivi.messaging.JmsRoutes.PUBLISH_STELLE_ON_WEBSITE_QUEUE_NAME;

import com.google.gson.Gson;
import de.thi.ivi.LoggerDelegate;
import de.thi.ivi.beans.Stellenausschreibung;
import java.util.logging.Logger;
import javax.jms.*;

/** @author seg0979 Sebastian Gaiser */
public class PublishStelleOnWebsite {

  private static final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

  public static void publishStelleOnWebsite(Stellenausschreibung stellenausschreibung) {
    try {
      Connection connection = JmsSessionFactory.createJmsConnection();
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

      Destination destination = session.createQueue(PUBLISH_STELLE_ON_WEBSITE_QUEUE_NAME);
      MessageProducer producer = session.createProducer(destination);
      producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

      TextMessage message = session.createTextMessage(new Gson().toJson(stellenausschreibung));
      LOGGER.info("Sending message: " + message.getText());
      producer.send(message);
      connection.close();

    } catch (JMSException e) {
      LOGGER.info(e.getMessage());
    }
  }
}
