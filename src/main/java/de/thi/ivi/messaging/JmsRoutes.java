package de.thi.ivi.messaging;

/** @author seg0979 Sebastian Gaiser */
public class JmsRoutes {

  public static final String ABSAGE_QUEUE_NAME = "absage.queue";
  public static final String ARBEITSVERTRAG_QUEUE_NAME = "arbeitsvertrag.queue";
  public static final String BEWERBER_AKZEPTIERT_QUEUE_NAME = "bewerberAkzeptiert.queue";
  public static final String INFOS_ERSTER_ARBEITSTAG_QUEUE_NAME = "infosErsterArbeitstag.queue";
  public static final String PUBLISH_STELLE_ON_WEBSITE_QUEUE_NAME = "publishOnWebsite.queue";
  public static final String STELLENAUSSCHREIBUNG_QUEUE_NAME = "stellenausschreibung.queue";
  public static final String TERMIN_QUEUE_NAME = "termin.queue";
  public static final String ZUSAGE_QUEUE_NAME = "zusage.queue";
}
