package de.thi.integration;

import de.thi.integration.routes.*;
import javax.jms.ConnectionFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.main.Main;

/** @author seg0979 Sebastian Gaiser */
public class MainApp {

  public static void main(String... args) throws Exception {
    final String BROKER_URL = "tcp://messaging:61616";

    Main main = new Main();

    System.out.println("Starting camel...");

    ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
    main.bind("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));

    main.configure().addRoutesBuilder(new RcvFromAbsageQueueRouteBuilder());
    main.configure().addRoutesBuilder(new RcvFromArbeitsvertragQueueRouteBuilder());
    main.configure().addRoutesBuilder(new RcvFromBewerberAkzeptiertQueueRouteBuilder());
    main.configure().addRoutesBuilder(new RcvFromInfosErsterArbeitstagQueueRouteBuilder());
    main.configure().addRoutesBuilder(new RcvFromPublishOnWebsiteQueueRouteBuilder());
    main.configure().addRoutesBuilder(new RcvFromStellenausschreibungQueueRouteBuilder());
    main.configure().addRoutesBuilder(new RcvFromTerminQueueRouteBuilder());
    main.configure().addRoutesBuilder(new ToPublishStellenausschreibungRouteBuilder());
    main.run(args);
  }
}
