package de.thi.integration.aggregators;

import de.thi.integration.models.Stellenausschreibung;
import java.util.Objects;
import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;

/** @author seg0979 Sebastian Gaiser */
public class IviAggregationStrategy implements AggregationStrategy {
  @Override
  public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
    if (oldExchange == null) {
      System.out.println("Aggregation: oldExchange is null");
      System.out.println(
          "Aggregation: newExchange is: "
              + newExchange.getIn().getBody(Stellenausschreibung.class));
      return newExchange;
    } else {
      System.out.println("Aggregation: oldExchange is not null");
      Stellenausschreibung oldStellenausschreibung =
          oldExchange.getIn().getBody(Stellenausschreibung.class);
      System.out.println("Aggregation: oldStellenausschreibung is: " + oldStellenausschreibung);
      Stellenausschreibung newStellenausschreibung =
          newExchange.getIn().getBody(Stellenausschreibung.class);
      System.out.println("Aggregation: newStellenausschreibung is: " + newStellenausschreibung);

      if ((oldStellenausschreibung.getStellenausschreibungwebsiteid() == null
              || oldStellenausschreibung.getStellenausschreibungwebsiteid().equals("nicht gesetzt"))
          && (newStellenausschreibung.getStellenausschreibungwebsiteid() != null
              || !Objects.equals(
                  newStellenausschreibung.getStellenausschreibungwebsiteid(), "nicht gesetzt"))) {
        System.out.println("Aggregation: updating websiteid");
        oldExchange.getIn().setBody(newStellenausschreibung);
      }
      return oldExchange;
    }
  }
}
