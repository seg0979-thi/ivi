package de.thi.integration.models;

import java.io.Serializable;

/** @author seg0979 Sebastian Gaiser */
public class Stellenausschreibung implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long stellenausschreibungid;

  private String stellenausschreibungname;
  private String stellenausschreibungbeschreibung;
  private String stellenausschreibungstatus;
  private String stellenausschreibungdringlichkeit;
  private String stellenausschreibungwebsiteid;
  private String processInstanceId;

  public Stellenausschreibung() {
    this.stellenausschreibungname = null;
    this.stellenausschreibungbeschreibung = null;
    this.stellenausschreibungstatus = null;
    this.stellenausschreibungdringlichkeit = null;
    this.stellenausschreibungwebsiteid = null;
    this.processInstanceId = null;
  }

  public Stellenausschreibung(
      String stellenausschreibungname,
      String stellenausschreibungbeschreibung,
      String stellenausschreibungstatus,
      String stellenausschreibungdringlichkeit,
      String stellenausschreibungwebsiteid,
      String processInstanceId) {
    this.stellenausschreibungname = stellenausschreibungname;
    this.stellenausschreibungbeschreibung = stellenausschreibungbeschreibung;
    this.stellenausschreibungstatus = stellenausschreibungstatus;
    this.stellenausschreibungdringlichkeit = stellenausschreibungdringlichkeit;
    this.stellenausschreibungwebsiteid = stellenausschreibungwebsiteid;
    this.processInstanceId = processInstanceId;
  }

  public Long getStellenausschreibungid() {
    return stellenausschreibungid;
  }

  public void setStellenausschreibungid(Long stellenausschreibungid) {
    this.stellenausschreibungid = stellenausschreibungid;
  }

  public String getStellenausschreibungname() {
    return stellenausschreibungname;
  }

  public void setStellenausschreibungname(String stellenausschreibungname) {
    this.stellenausschreibungname = stellenausschreibungname;
  }

  public String getStellenausschreibungbeschreibung() {
    return stellenausschreibungbeschreibung;
  }

  public void setStellenausschreibungbeschreibung(String stellenausschreibungbeschreibung) {
    this.stellenausschreibungbeschreibung = stellenausschreibungbeschreibung;
  }

  public String getStellenausschreibungstatus() {
    return stellenausschreibungstatus;
  }

  public void setStellenausschreibungstatus(String stellenausschreibungstatus) {
    this.stellenausschreibungstatus = stellenausschreibungstatus;
  }

  public String getStellenausschreibungdringlichkeit() {
    return stellenausschreibungdringlichkeit;
  }

  public void setStellenausschreibungdringlichkeit(String stellenausschreibungdringlichkeit) {
    this.stellenausschreibungdringlichkeit = stellenausschreibungdringlichkeit;
  }

  public String getStellenausschreibungwebsiteid() {
    return stellenausschreibungwebsiteid;
  }

  public void setStellenausschreibungwebsiteid(String stellenausschreibungwebsiteid) {
    this.stellenausschreibungwebsiteid = stellenausschreibungwebsiteid;
  }

  public String getProcessInstanceId() {
    return processInstanceId;
  }

  public void setProcessInstanceId(String processInstanceId) {
    this.processInstanceId = processInstanceId;
  }

  @Override
  public String toString() {
    return "Stellenausschreibung{"
        + "stellenausschreibungid="
        + stellenausschreibungid
        + ", stellenausschreibungname='"
        + stellenausschreibungname
        + '\''
        + ", stellenausschreibungbeschreibung='"
        + stellenausschreibungbeschreibung
        + '\''
        + ", stellenausschreibungstatus='"
        + stellenausschreibungstatus
        + '\''
        + ", stellenausschreibungdringlichkeit='"
        + stellenausschreibungdringlichkeit
        + '\''
        + ", stellenausschreibungwebsiteid='"
        + stellenausschreibungwebsiteid
        + '\''
        + ", processInstanceId='"
        + processInstanceId
        + '\''
        + '}';
  }
}
