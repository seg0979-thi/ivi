package de.thi.integration.models;

import java.io.Serializable;
import java.util.UUID;

/** @author seg0979 Sebastian Gaiser */
public class WebsiteResponse implements Serializable {
  UUID uuid;
  String stelle;

  public WebsiteResponse() {
    this.uuid = null;
    this.stelle = null;
  }

  public WebsiteResponse(UUID uuid, String stelle) {
    this.uuid = uuid;
    this.stelle = stelle;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  public String getStelle() {
    return stelle;
  }

  public void setStelle(String stelle) {
    this.stelle = stelle;
  }

  @Override
  public String toString() {
    return "WebsiteResponse{" + "uuid=" + uuid + ", stelle='" + stelle + '\'' + '}';
  }
}
