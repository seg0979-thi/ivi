package de.thi.integration.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/** @author tog8596 Tobias Gaul */
@XmlRootElement(name = "stellenausschreibung")
@XmlAccessorType(XmlAccessType.FIELD)
public class StellenausschreibungXML extends Stellenausschreibung {
  public StellenausschreibungXML() {
    super();
  }
}
