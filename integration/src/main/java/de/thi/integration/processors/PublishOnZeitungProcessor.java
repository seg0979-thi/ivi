package de.thi.integration.processors;

import de.thi.integration.models.Stellenausschreibung;
import org.apache.camel.Exchange;

/** @author seg0979 Sebastian Gaiser */
public class PublishOnZeitungProcessor implements org.apache.camel.Processor {

  @Override
  public void process(Exchange exchange) throws Exception {
    Stellenausschreibung stellenausschreibung =
        exchange.getIn().getBody(Stellenausschreibung.class);

    System.out.println("Debug PublishOnZeitungProcessor: " + stellenausschreibung);
  }
}
