package de.thi.integration.processors;

import de.thi.integration.models.Stellenausschreibung;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.json.JSONObject;

/** @author seg0979 Sebastian Gaiser */
public class CamundaBestaetigungProcessor implements Processor {

  @Override
  public void process(Exchange exchange) throws Exception {
    Stellenausschreibung stellenausschreibung =
        exchange.getIn().getBody(Stellenausschreibung.class);

    JSONObject json = new JSONObject();
    json.put("messageName", "Bestaetigung");
    json.put("processInstanceId", stellenausschreibung.getProcessInstanceId());
    json.put("resultEnabled", true);

    JSONObject stellenausschreibungwebsiteid = new JSONObject();
    stellenausschreibungwebsiteid.put(
        "value", stellenausschreibung.getStellenausschreibungwebsiteid());
    stellenausschreibungwebsiteid.put("type", "String");

    JSONObject processVariables = new JSONObject();
    processVariables.put("stellenausschreibungwebsiteid", stellenausschreibungwebsiteid);

    json.put("processVariables", processVariables);

    System.out.println("Debug jsonString: " + json.toString());
    exchange.getIn().setHeader(Exchange.HTTP_METHOD, "POST");
    exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/json");
    exchange.getIn().setBody(json.toString());
    System.out.println(
        "Debug exchange before sending to Camunda: "
            + exchange.getIn().getBody()
            + " headers: "
            + exchange.getIn().getHeaders());
  }
}
