package de.thi.integration.processors;

import static de.thi.integration.models.StellenauschreibungStati.AUSGESCHRIEBEN;
import static de.thi.integration.models.StellenauschreibungStati.NICHT_AUSGESCHRIEBEN;

import com.google.gson.Gson;
import de.thi.integration.models.Stellenausschreibung;
import de.thi.integration.models.WebsiteResponse;
import java.nio.charset.StandardCharsets;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

/** @author seg0979 Sebastian Gaiser */
public class PublishOnWebsiteProcessor implements Processor {

  final String MOCK_URL = "http://mock:8090";
  final String AUSSCHREIBEN = "/stelle";
  final String SCHLIESSEN = AUSSCHREIBEN + "/close";

  @Override
  public void process(Exchange exchange) throws Exception {
    Stellenausschreibung stellenausschreibung =
        exchange.getIn().getBody(Stellenausschreibung.class);

    //    System.out.println("Headers: " + exchange.getIn().getHeaders());
    System.out.println("PublishOnWebsiteProcessor: " + stellenausschreibung);
    JSONObject json = new JSONObject();

    try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
      String calledUri = "";
      if (stellenausschreibung
          .getStellenausschreibungstatus()
          .equals(NICHT_AUSGESCHRIEBEN.toString())) {
        json.put("name", stellenausschreibung.getStellenausschreibungname());
        calledUri = MOCK_URL + AUSSCHREIBEN;
      } else if (stellenausschreibung
          .getStellenausschreibungstatus()
          .equals(AUSGESCHRIEBEN.toString())) {
        json.put("websiteuuid", stellenausschreibung.getStellenausschreibungwebsiteid());
        calledUri = MOCK_URL + SCHLIESSEN;
      }
      HttpPost httpMethod = new HttpPost(calledUri);
      StringEntity params = new StringEntity(json.toString());

      httpMethod.addHeader("Content-type", "application/json");
      httpMethod.setEntity(params);

      HttpResponse response = httpClient.execute(httpMethod);
      System.out.println("Response Code: " + response.getStatusLine().getStatusCode());

      String result = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
      System.out.println("Response Content: " + result);
      response.getEntity().getContent().close();

      WebsiteResponse websiteResponse = null;
      if (stellenausschreibung
          .getStellenausschreibungstatus()
          .equals(NICHT_AUSGESCHRIEBEN.toString())) {
        websiteResponse = new Gson().fromJson(result, WebsiteResponse.class);
      } else if (stellenausschreibung
          .getStellenausschreibungstatus()
          .equals(AUSGESCHRIEBEN.toString())) {
        websiteResponse = new Gson().fromJson(result, WebsiteResponse.class);
      }

      if (websiteResponse != null) {
        System.out.println("WebsiteResponse ID: " + websiteResponse.getUuid().toString());
        stellenausschreibung.setStellenausschreibungwebsiteid(websiteResponse.getUuid().toString());
      } else {
        System.out.println("WebsiteResponse ID is null");
      }
    } catch (Exception ex) {
      System.out.println("Exception : " + ex);
    }

    exchange.getIn().setBody(stellenausschreibung);
  }
}
