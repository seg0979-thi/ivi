package de.thi.integration.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.json.JSONObject;

/** @author seg0979 Sebastian Gaiser */
public class CamundaBewerberProcessor implements Processor {

  @Override
  public void process(Exchange exchange) throws Exception {
    // TODO use Bewerbung
    //    Bewerbung bewerbung =
    //      exchange.getIn().getBody(Bewerbung.class);

    JSONObject json = new JSONObject();
    json.put("messageName", "Mitarbeiter");
    //    json.put("processInstanceId", bewerbung.getProcessInstanceId());
    System.out.println("Debug jsonString: " + json.toString());

    exchange.getOut().setHeader(Exchange.HTTP_METHOD, "POST");
    exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
    exchange.getOut().setBody(json.toString());
  }
}
