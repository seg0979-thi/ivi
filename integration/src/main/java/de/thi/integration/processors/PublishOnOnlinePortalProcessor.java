package de.thi.integration.processors;

import de.thi.integration.models.Stellenausschreibung;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/** @author seg0979 Sebastian Gaiser */
public class PublishOnOnlinePortalProcessor implements Processor {

  @Override
  public void process(Exchange exchange) throws Exception {
    Stellenausschreibung stellenausschreibung =
        exchange.getIn().getBody(Stellenausschreibung.class);

    System.out.println("Debug PublishOnOnlinePortalProcessor: " + stellenausschreibung);
  }
}
