package de.thi.integration.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.gson.GsonDataFormat;

/** @author seg0979 Sebastian Gaiser */
public class RcvFromInfosErsterArbeitstagQueueRouteBuilder extends RouteBuilder {

  public static final String INFOS_ERSTER_ARBEITSTAG_QUEUE_NAME = "infosErsterArbeitstag.queue";

  @Override
  public void configure() throws Exception {
    from("jms:queue:" + INFOS_ERSTER_ARBEITSTAG_QUEUE_NAME)
        .log("Received message from InfosErsterArbeitstag Queue: ${body}")
        .unmarshal(new GsonDataFormat(String.class))
        .log("Unmarshalled Infos: ${body}")
        .to("direct:infosErsterArbeitstag");

    from("direct:infosErsterArbeitstag").to("log:out");
  }
}
