package de.thi.integration.routes;

import de.thi.integration.models.Stellenausschreibung;
import de.thi.integration.models.StellenausschreibungXML;
import de.thi.integration.processors.PublishOnWebsiteProcessor;
import javax.xml.bind.JAXBContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.gson.GsonDataFormat;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.converter.jaxb.JaxbDataFormat;

/** @author seg0979 Sebastian Gaiser */
/** @author tog8596 Tobias Gaul */
public class ToPublishStellenausschreibungRouteBuilder extends RouteBuilder {

  @Override
  public void configure() throws Exception {
    // XML Data Format
    JaxbDataFormat xmlDataFormat = new JaxbDataFormat();
    JAXBContext con = JAXBContext.newInstance(StellenausschreibungXML.class);
    xmlDataFormat.setContext(con);

    // JSON Data Format
    JacksonDataFormat jsonDataFormat = new JacksonDataFormat(StellenausschreibungXML.class);

    from("direct:publishStelleOnWebsite")
        .log("publishStelleOnWebsite: ${body}")
        .log("publishStelleOnWebsite: Headers: ${headers}")
        .unmarshal(new GsonDataFormat(Stellenausschreibung.class))
        .log("publishStelleOnWebsite: Unmarshalled Stellenausschreibung: ${body}")
        .process(new PublishOnWebsiteProcessor())
        .log("publishStelleOnWebsite: Published Stellenausschreibung on Website: ${body}")
        .marshal(new GsonDataFormat(Stellenausschreibung.class))
        .log("publishStelleOnWebsite: Marshalled Stellenausschreibung: ${body}")
        .log("publishStelleOnWebsite: Headers: ${headers}")
        .log("End publishStelleOnWebsite -------------------------------------------------")
        .to("direct:stelleAusgeschrieben");

    from("direct:publishStelleOnOnlinePortal")
        .log("publishStelleOnOnlinePortal: ${body}")
        .unmarshal(jsonDataFormat)
        .log("publishStelleOnOnlinePortal: Unmarshalled Stellenausschreibung: ${body}")
        .to("file:ausschreibungen/linkedin?fileName=${headers.id}_stellenausschreibung.xml")
        .log(
            "publishStelleOnOnlinePortal: Stellenausschreibung persistiert unter"
                + " /ausschreibungen/linkedin/${headers.id}_stellenausschreibung.xml")
        .marshal(jsonDataFormat)
        .log("publishStelleOnOnlinePortal: Marshalled Stellenausschreibung: ${body}")
        .to("direct:stelleAusgeschrieben");

    from("direct:publishStelleOnZeitung")
        .log("publishStelleOnZeitung: ${body}")
        .unmarshal(jsonDataFormat)
        .log("publishStelleOnZeitung: Unmarshalled Stellenausschreibung: ${body}")
        .to("file:ausschreibungen/zeitung?fileName=${headers.id}_stellenausschreibung.xml")
        .log(
            "publishStelleOnZeitung: Stellenausschreibung persistiert unter"
                + " /ausschreibungen/zeitung/${headers.id}_stellenausschreibung.xml")
        .marshal(jsonDataFormat)
        .log("publishStelleOnZeitung: Marshalled Stellenausschreibung: ${body}")
        .to("direct:stelleAusgeschrieben");
  }
}
