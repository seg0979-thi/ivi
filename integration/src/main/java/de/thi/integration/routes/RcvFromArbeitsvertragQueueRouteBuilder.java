package de.thi.integration.routes;

import org.apache.camel.builder.RouteBuilder;

/** @author ane3105 Andrej Elokhov */
public class RcvFromArbeitsvertragQueueRouteBuilder extends RouteBuilder {

  public static final String ARBEITSVERTRAG_QUEUE_NAME = "arbeitsvertrag.queue";

  @Override
  public void configure() throws Exception {
    from("jms:queue:" + ARBEITSVERTRAG_QUEUE_NAME)
        .log("Received message from Arbeitsvertrag Queue: ${body}")
        .to("log:out");
  }
}
