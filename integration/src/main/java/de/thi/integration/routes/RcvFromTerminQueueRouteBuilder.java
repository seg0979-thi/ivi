package de.thi.integration.routes;

import org.apache.camel.builder.RouteBuilder;

/** @author tog8596 Tobias Gaul */
public class RcvFromTerminQueueRouteBuilder extends RouteBuilder {

  public static final String TERMIN_QUEUE_NAME = "termin.queue";

  @Override
  public void configure() throws Exception {
    from("jms:queue:" + TERMIN_QUEUE_NAME)
        .log("Received message from Termin Queue: ${body}")
        .to("log:out");
  }
}
