package de.thi.integration.routes;

import de.thi.integration.aggregators.IviAggregationStrategy;
import de.thi.integration.models.Stellenausschreibung;
import de.thi.integration.processors.CamundaBestaetigungProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.gson.GsonDataFormat;

/** @author seg0979 Sebastian Gaiser */
public class RcvFromPublishOnWebsiteQueueRouteBuilder extends RouteBuilder {

  public static final String PUBLISH_STELLE_ON_WEBSITE_QUEUE_NAME = "publishOnWebsite.queue";

  @Override
  public void configure() throws Exception {
    from("direct:stelleAusgeschrieben")
        .log("stelleAusgeschrieben: ${body}")
        .log("stelleAusgeschrieben: Headers: ${headers}")
        .unmarshal(new GsonDataFormat(Stellenausschreibung.class))
        .log("stelleAusgeschrieben: Unmarshalled Stellenausschreibung: ${body}")
        .log("stelleAusgeschrieben: Headers: ${headers}")
        .aggregate(header("id"), new IviAggregationStrategy())
        .completionSize(header("COUNTER"))
        .log("AGGREGATOR completed ${headers}")
        .log("AGGREGATOR completed ${body}")
        .process(new CamundaBestaetigungProcessor())
        .log("CamundaBestaetigungProcessor completed ${headers}")
        .log("CamundaBestaetigungProcessor completed ${body}")
        .to("http://camunda:8080/engine-rest/message");
  }
}
