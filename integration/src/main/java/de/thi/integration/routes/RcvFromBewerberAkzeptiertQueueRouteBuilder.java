package de.thi.integration.routes;

import de.thi.integration.processors.CamundaBewerberProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.gson.GsonDataFormat;

/** @author seg0979 Sebastian Gaiser */
public class RcvFromBewerberAkzeptiertQueueRouteBuilder extends RouteBuilder {

  public static final String BEWERBER_AKZEPTIERT_QUEUE_NAME = "bewerberAkzeptiert.queue";

  // TODO work with Bewerbung
  @Override
  public void configure() throws Exception {
    from("jms:queue:" + BEWERBER_AKZEPTIERT_QUEUE_NAME)
        .log("Received message from BewerberAkzeptiert Queue: ${body}")
        .unmarshal(new GsonDataFormat(String.class))
        .log("Unmarshalled Bewerbung: ${body}")
        .to("direct:bewerbungAkzeptiert");

    from("direct:bewerbungAkzeptiert")
        .log("Bewerbung akzeptiert: ${body}")
        .log("Debug: " + "${body}" + " with PID: " + "${body.getProcessInstanceId()}")
        .process(new CamundaBewerberProcessor())
        .to("http://camunda:8080/engine-rest/message");
  }
}
