package de.thi.integration.routes;

import de.thi.integration.models.Stellenausschreibung;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.gson.GsonDataFormat;

/** @author seg0979 Sebastian Gaiser */
public class RcvFromStellenausschreibungQueueRouteBuilder extends RouteBuilder {

  public static final String STELLENAUSSCHREIBUNG_QUEUE_NAME = "stellenausschreibung.queue";

  @Override
  public void configure() throws Exception {
    from("jms:queue:" + STELLENAUSSCHREIBUNG_QUEUE_NAME)
        .log("Received message from Stellenausschreibung Queue: ${body}")
        .unmarshal(new GsonDataFormat(Stellenausschreibung.class))
        .process(
            new Processor() {
              @Override
              public void process(Exchange exchange) throws Exception {
                Stellenausschreibung stellenausschreibung =
                    exchange.getIn().getBody(Stellenausschreibung.class);
                System.out.println(stellenausschreibung);
                exchange
                    .getIn()
                    .setHeader(
                        "dringlichkeit",
                        stellenausschreibung.getStellenausschreibungdringlichkeit());
                exchange.getIn().setHeader("id", stellenausschreibung.getStellenausschreibungid());
                exchange.getIn().setBody(stellenausschreibung);
              }
            })
        .marshal(new GsonDataFormat(Stellenausschreibung.class))
        .log("Marshal: ${body}")
        .log("Header: ${headers}")
        .choice()
        .when(header("dringlichkeit").isEqualTo("high"))
        .log("stellenausschreibungdringlichkeit = high")
        .to("direct:high")
        .when(header("dringlichkeit").isEqualTo("medium"))
        .log("stellenausschreibungdringlichkeit = medium")
        .to("direct:medium")
        .when(header("dringlichkeit").isEqualTo("low"))
        .log("stellenausschreibungdringlichkeit = low")
        .to("direct:low")
        .otherwise()
        .log("No Route matched")
        .endChoice();

    from("direct:high")
        .log("Debug: high")
        .setHeader("COUNTER", constant(3))
        .multicast()
        .to(
            "direct:publishStelleOnWebsite",
            "direct:publishStelleOnOnlinePortal",
            "direct:publishStelleOnZeitung");

    from("direct:medium")
        .log("Debug: medium")
        .setHeader("COUNTER", constant(2))
        .multicast()
        .to("direct:publishStelleOnWebsite", "direct:publishStelleOnOnlinePortal");

    from("direct:low")
        .log("Debug: low")
        .setHeader("COUNTER", constant(1))
        .to("direct:publishStelleOnWebsite");
  }
}
