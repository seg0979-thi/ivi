package de.thi.integration.routes;

import org.apache.camel.builder.RouteBuilder;

/** @author tog8596 Tobias Gaul */
public class RcvFromAbsageQueueRouteBuilder extends RouteBuilder {

  public static final String ABSAGE_QUEUE_NAME = "absage.queue";

  @Override
  public void configure() throws Exception {
    from("jms:queue:" + ABSAGE_QUEUE_NAME)
        .log("Received message from Absage Queue: ${body}")
        .to("log:out");
  }
}
