# Created by seg0979; Sebastian Gaiser

.PHONY: help

.DEFAULT_GOAL := all

SHELL := /bin/bash

build:
	    @docker-compose build

up: build
	    @docker-compose up -d

down:
	    @docker-compose down

logs:
	    @docker-compose logs

logs-f:
	    @docker-compose logs -f

pre-commit:
	    @pre-commit install

pre-commit-run: pre-commit
	    @pre-commit run -a

clean: up pre-commit
	    @./scripts/clean.sh

camel: up pre-commit

clean-all:
	    @docker-compose down -v

deploy: up clean
	    @./scripts/deploy.sh

init: pre-commit
	    @./scripts/run.sh

start-bewerbung:
	    @./scripts/start-bewerbung.sh

stellenausschreibung-bestaetigen:
	    @./scripts/stellenausschreibung-bestaetigen.sh

akzeptanz-bewerber:
	    @./scripts/akzeptanz-bewerber.sh

help:
	    @echo "Makefile commands:"
	    @echo "help"
	    @echo "deploy"
	    @echo "init"
	    @echo "all"
	    @echo "pre-commit"

all: deploy
